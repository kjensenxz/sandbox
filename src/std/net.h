/*
 * net.h - some useful network functions/wrappers
 *
 * Copyright @kjensenxz/Kenneth B. Jensen, 2021. <kenneth@jensen.cf>
 * This library is licensed under the GNU GPL, version 3 or later.
 * See the LICENSE.
 */

#ifndef _NET_H_
#define _NET_H_

int net_connect(char *host, char *service);
int net_listen(char *host, char *service);
int net_accept4(int server, int flags);
int net_accept(int server);
int net_close(int sock);

#endif /* _NET_H_ */
