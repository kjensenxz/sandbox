/*
 * util.h - some useful utlity functions/macros, mostly logging stuff
 *
 * Copyright @kjensenxz/Kenneth B. Jensen, 2021. <kenneth@jensen.cf>
 * This library is licensed under the GNU GPL, version 3 or later.
 * See the LICENSE.
 */

#ifndef _UTIL_H_
#define _UTIL_H_
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

/*
 * string functions
 */
#define strlncmp(BIG, LITTLE) strncmp(BIG, LITTLE, strlen(LITTLE))
#define match(BIG, LITTLE) (0 == strlncmp(BIG, LITTLE))
#define chop(STR) STR[strlen(STR)-1] = '\0'
static inline char *strdupchk(const char *s);   /* deferred for ERR() */

/*
 * heap functions
 */
#define checkfree(PTR)                  \
	do {                            \
		if (NULL != PTR) {      \
			free(PTR);      \
		}                       \
		PTR = NULL;             \
	} while(0);

#define growarray(ARRAY, COUNTER, SIZE, AMOUNT)                 \
	do {                                                    \
		COUNTER += AMOUNT;                              \
		ARRAY = realloc(ARRAY, COUNTER * SIZE);         \
		if (NULL == ARRAY) {                            \
			FATAL("realloc: %s", errno_s);          \
		}                                               \
	} while (0);                                            \

#define checkarray(ARRAY, SIZE, ERRSTAT)                \
	for (int _I_ = 0; _I_ < SIZE; ++_I_) {          \
		if (NULL == ARRAY[_I_]) {               \
			ERRSTAT;                        \
		}                                       \
	}

#define freearray(ARRAY, SIZE)                          \
	for (int _I_ = 0; _I_ < SIZE; ++_I_) {          \
		checkfree(ARRAY[_I_]);                  \
	}

/*
 * I/O functions
 */
#define nwrite(FD, STR) write(FD, STR, strlen(STR))
/* "tee" dprintf to stdio*/
static inline int
tdprintf(int fd, const char *fmt, ...) {
	va_list         ap;
	int             rv = -1;

	va_start(ap, fmt);

	vprintf(fmt, ap);
	rv = vdprintf(fd, fmt, ap);

	va_end(ap);
	return rv;
}

#define errno_s (strerror(errno))

/* ISO 8601 format plus null-terminator -> 25 chars */
#define TIMELEN 25
static char TIME[TIMELEN] = {0};
static inline char *
GETTIME(void) {
	time_t          t = {0};
	struct tm       *tmp = NULL;
	size_t          rv = 0;

	t = time(NULL);
	tmp = localtime(&t);
	if (NULL == tmp) {
		perror("WARNING: localtime() failed");
		return NULL;
	}

	/*
	 * strftime(3) expects the inclusion of the terminating null byte, and
	 * does not set errno as defined by POSIX.1-2001.
	 */
	rv = strftime(TIME, TIMELEN, "%FT%T%z", tmp);
	if (0 == rv) {
		fprintf(stderr, "WARNING: strftime in GETTIME() returned 0!\n");
	}

	return TIME;
}
#undef TIMELEN

/* all logging facilities. */
static inline void
_LOG(FILE *file, char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	vfprintf(file, fmt, ap);

	va_end(ap);
}

#define LOG(file, fmt, ...) \
	_LOG(file, "%s " fmt "\n", GETTIME() __VA_OPT__(,) __VA_ARGS__)

#define die(msg) do {                                                   \
	LOG(stderr, "FATAL: %s: %s. Exiting.", msg, strerror(errno)); \
	exit(EXIT_FAILURE);                                             \
} while (0)

/* enable __DEBUG__ with compiler (e.g. -D __DEBUG__) when building. */
#ifdef __DEBUG__
#define DEBUG(...) LOG(stderr, "DEBUG: " __VA_ARGS__)
#else
#define DEBUG(...)
#endif /* __DEBUG__ */

#define INFO(...) LOG(stderr, "INFO: " __VA_ARGS__)

#define NOTICE(...) LOG(stderr, "NOTICE: " __VA_ARGS__)

#define ERR(...) LOG(stderr, "ERR: " __VA_ARGS__)

#define WARN(...) LOG(stderr, "WARN: " __VA_ARGS__)

#define FATAL(...) LOG(stderr, "FATAL: " __VA_ARGS__)

static inline char *
strdupchk(const char *s) {
	char            *rv = NULL;

	rv = strdup(s);
	if (NULL == rv) {
		ERR("strdupchk: %s", errno_s);
	}
	return rv;
}

#endif /* _UTIL_H_ */
