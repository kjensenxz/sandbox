/*
 * net.c - some useful network functions/wrappers
 *
 * Copyright @kjensenxz/Kenneth B. Jensen, 2021. <kenneth@jensen.cf>
 * This library is licensed under the GNU GPL, version 3 or later.
 * See the LICENSE.
 */

#define _GNU_SOURCE     /* for accept4(2) */
#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

/*
 * net.h & util.h should reside in the same directory (std/).
 */
#include "net.h"
#include "util.h"

#define BACKLOG 10

static inline void *get_in_addr(struct sockaddr *sa);

/*
 * get_ip returns the formatted socket IP (i.e. [::1]:1234 for IPv6), or NULL
 * on error.
 */

#define get_ip(ADDRINFO, DST)                                                  \
	do {                                                                   \
		inet_ntop(ADDRINFO->ai_family,                                 \
		          get_in_addr((struct sockaddr *)ADDRINFO->ai_addr),   \
		          DST, INET6_ADDRSTRLEN);                              \
		if (AF_INET6 == ADDRINFO->ai_family) {                         \
			memmove(&DST[1], DST, INET6_ADDRSTRLEN);               \
			DST[0] = '[';                                          \
			s[strnlen(s, INET6_ADDRSTRLEN)] = ']';                 \
		}                                                              \
	}                                                                      \
	while(0)

/* 
 * get sockaddr, IPv4 or IPv6. thanks, Beej!
 */

static inline void *
get_in_addr(struct sockaddr *sa) {
	assert(sa->sa_family == AF_INET || sa->sa_family == AF_INET6);

	if (AF_INET == sa->sa_family) {
		return &(((struct sockaddr_in *)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

/*
 * net_connect returns a connected socket, or -1 on error; with messages.
 */

int
net_connect(char *host, char *service) {
	struct addrinfo hints = {
	                .ai_family = AF_UNSPEC,
	                .ai_socktype = SOCK_STREAM,
	};
	struct addrinfo *svinfo = NULL,
	                *p = NULL;
	int             sock = -1,
	                attempts = 0,
	                rv = -1;
	/* INET6_ADDRSTRLEN+2 for IPv6 brackets */
	char            s[INET6_ADDRSTRLEN+2] = {0},
	                *s_ = NULL;

	if (NULL == host || NULL == service) {
		return -1;
	}

	rv = getaddrinfo(host, service, &hints, &svinfo);
	if (0 != rv) {
		FATAL("net_connect: failed looking up %s! getaddrinfo: %s.",
		      host, gai_strerror(rv));
		return -1;
	}

	for (p = svinfo; p != NULL; p = p->ai_next) {
		get_ip(p, s);
		INFO("net_connect: connecting to %s:%s...",
		     s, service);

		sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (0 > sock) {
			attempts += 1;
			WARN("net_connect: socket initialization failed: %s!",
			     strerror(errno));
			continue;
		}
		rv = connect(sock, p->ai_addr, p->ai_addrlen);
		if (0 > rv) {
			attempts += 1;
			WARN("net_connect: connecting to %s:%s failed: %s",
			     s, service, strerror(errno));
			close(sock);
			continue;
		}
		
		break;
	}

	freeaddrinfo(svinfo);

	if (p == NULL) {
		FATAL("net_connect: Failed to connect! (%d attempts)",
		      attempts);
		return -1;
	}

	if (NULL != s_) {
		INFO("net_connect: connected to %s:%s\n", s, service);
	}
	return sock;
}

/*
 * net_listen returns a listening socket, or -1 on error; with messages.
 */

int
net_listen(char *host, char *service) {
	struct addrinfo hints = {
	                .ai_family = AF_UNSPEC,
	                .ai_socktype = SOCK_STREAM,
	                .ai_flags = AI_PASSIVE
	};
	struct addrinfo *svinfo = NULL,
	                *p = NULL;
	int             server = -1,
	                yes = 1,
	                rv = -1;

	if (NULL == host || NULL == service) {
		return -1;
	}

	rv = getaddrinfo(NULL, service, &hints, &svinfo);
	if (0 != rv) {
		WARN("net_listen: could not look up %s! getaddrinfo: %s.",
		     host, gai_strerror(rv));
		return -1;
	}

	for (p = svinfo; NULL != p; p = p->ai_next) {
		server = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (0 > server) {
			WARN("net_listen: socket initialization failed: %s!",
			     strerror(errno));
			continue;
		}
		rv = setsockopt(server, SOL_SOCKET, SO_REUSEADDR, 
		                &yes, sizeof(yes));

		if (0 > rv) {
			close(server);
			WARN("net_listen: setsockopt failed: %s",
			        strerror(errno)
			);
			continue;
		}

		rv = bind(server, p->ai_addr, p->ai_addrlen);
		if (0 > rv) {
			close(server);
			WARN("net_listen: could not bind to %s:%s: %s",
				strerror(errno)
			);
			continue;
		}

		break;
	}
	if (NULL == p) {
		freeaddrinfo(svinfo);
		FATAL("net_listen: failed to bind to %s:%s: %s",
		        host, service, strerror(errno)
		);
		return -1;
	}

	rv = listen(server, BACKLOG);
	if (0 > rv) {
		close(server);
		freeaddrinfo(svinfo);
		FATAL("net_listen: failed to listen on %s:%s: %s",
		        host, service, strerror(errno)
		);
		return -1;
	}

	freeaddrinfo(svinfo);
	return server;
}

/*
 * net_accept wraps accept4(2) and returns client socket or -1 on error; with 
 * messages.
 */

int
net_accept4(int server, int flags) {
	struct sockaddr_storage clnt_addr = {0};
	socklen_t               addrlen = {0};
	char                    clntIP[INET6_ADDRSTRLEN] = {0};
	int                     client = -1;

	assert(0 <= server);

	addrlen = sizeof(clnt_addr);
	client = accept4(server, (struct sockaddr *)&clnt_addr, &addrlen, flags);
	
	if (0 > client) {
		return -1;
	}

	inet_ntop(clnt_addr.ss_family,
	          get_in_addr((struct sockaddr*)&clnt_addr),
	          clntIP, INET6_ADDRSTRLEN
	);

	INFO("net_accept: new connection from %s.", clntIP);
	DEBUG("net_accept: %s bound to fd %d.", clntIP, client);

	return client;
}

int
net_accept(int server) {
	return net_accept4(server, 0);
}

/*
 * net_close is a simple wrapper around close(2).
 */

int
net_close(int sock) {
	return close(sock);
}
