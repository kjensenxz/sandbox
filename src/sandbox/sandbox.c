/*
 * sandbox.c
 *
 * Sandbox is licensed under the GNU GPL, version 3 or later.
 * See the LICENSE.
 * Copyright (C) Kenneth B. Jensen 2021. All rights reserved.
 *
 * This program is a prototype implementation of the RFC
 * contained in the root directory.
 */

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sandbox.h"
#include "param.h"
#include "std/net.h"
#include "std/util.h"

_Noreturn void usage(char *argv, int exitcode);
int sandbox_reset(char *config_path, char *qmp_port, int console_port);
int sandbox_start(char *config_path, char *qmp_port, int console_port);
int sandbox_stop(char *qmp_port);

_Noreturn void
usage(char *argv0, int exitcode) {
	printf(
	"%s start <config_path> <qmp_port> <console_port>\n"
	"%s reset <config_path> <qmp_port> <console_port>\n"
	"%s stop <qmp_port>\n"
	"It is in error to call stop or reset without a running instance.\n"
	"This program is free software, licensed under the GNU GPL, version 3 or later. Copyright kjensenxz 2021.\n",
	argv0, argv0, argv0
	);

	exit(exitcode);
}

#define BUFLEN 256
int
sandbox_stop(char *qmp_port) {
	int             sock = -1,
	                flag = -1;
	char            buf[BUFLEN] = {0};
	char *const     quit_msg =
	                        " { \"execute\": \"qmp_capabilities\" } "
	                        " { \"execute\": \"quit\" } ";

	sock = net_connect("localhost", qmp_port);
	if (0 > sock) {
		FATAL("sandbox_stop: unable to connect to localhost:%s\n", 
		      qmp_port
		);
		return EXIT_FAILURE;
	}

	flag = 1;
	while (read(sock, buf, BUFLEN)) {
		INFO("QMP %d (in): %s", qmp_port, buf);
		if (flag) {
			flag = 0;
			INFO("QMP %d (out): %s", qmp_port, quit_msg);
			nwrite(sock, quit_msg);
		}
	}

	net_close(sock);

	return EXIT_SUCCESS;
}
#undef BUFLEN


int
sandbox_start(char *config_path, char *qmp_port, int console_port) {
	char            **myparams = NULL;
	int             i = -1,
	                rv = -1;

	myparams = get_startparams(config_path, qmp_port, console_port);
	if (NULL == myparams) {
		FATAL("sandbox_start: get_startparams returned NULL.");
		return EXIT_FAILURE;
	}

	for (i = 0; NULL != myparams[i]; ++i) {
		printf("%s ", myparams[i]);
	}
	printf("\n");

	rv = execvp(QEMU_SYSTEM_BIN, myparams);

	/*
	 * if execvp() is successful, the rest of this code is not executed.
	 */
	if (0 > rv) {
		FATAL("sandbox_start: execvp: %s", strerror(errno));
	}

	for (i = 0; NULL != myparams[i] && START_ARGC > i; ++i) {
		free(myparams[i]);
	}
	free(myparams);
	return EXIT_FAILURE;
}

int
sandbox_reset(char *config_path, char *qmp_port, int console_port) {
	int             rv = -1;

	rv = sandbox_stop(qmp_port);
	if (EXIT_SUCCESS != rv) {
		FATAL("sandbox_reset: could not stop QMP port %s!\n", qmp_port);
		return EXIT_FAILURE;
	}

	rv = sandbox_start(config_path, qmp_port, console_port);
	return rv;
}

int
main(int argc, char *argv[]) {
	char            *cmd = argv[1],
	                *config_path = NULL,
	                *qmp_port = NULL;       /* char* for net functions */
	int             console_port = -1,
	                opt = -1;

	if (STOP_ARGC-1 > argc) {
		usage(argv[0], EXIT_FAILURE);
	}

	opt = getopt(argc, argv, "Hh?");
	if (0 < opt) {
		usage(argv[0], EXIT_SUCCESS);
	}

	cmd = argv[1];
	if (match(cmd, "reset")) {
		if (RESET_ARGC-1 > argc) {
			usage(argv[0], EXIT_FAILURE);
		}
		config_path = argv[2];
		qmp_port = argv[3];
		console_port = atoi(argv[4]);
		return sandbox_reset(config_path, qmp_port, console_port);
	}
	else if (match(cmd, "start")) {
		if (START_ARGC-1 > argc) {
			usage(argv[0], EXIT_FAILURE);
		}
		config_path = argv[2];
		qmp_port = argv[3];
		console_port = atoi(argv[4]);
		return sandbox_start(config_path, qmp_port, console_port);
	}
	else if (match(cmd, "stop")) {
		qmp_port = argv[2];
		return sandbox_stop(qmp_port);
	}

	usage(argv[0], EXIT_FAILURE);
}

#undef START_ARGC
#undef STOP_ARGC
#undef RESET_ARGC
