/*
 * param.h
 *
 * Sandbox is licensed under the GNU GPL, version 3 or later.
 * See the LICENSE.
 * Copyright (C) Kenneth B. Jensen 2021. All rights reserved.
 */

#ifndef _PARAM_H_
#define _PARAM_H_

#define RESET_ARGC      6
#define START_ARGC      6
#define STOP_ARGC       4

char **get_startparams(char *config_path, char *qmp_port, int console_port);

#endif /* _PARAM_H_ */
