/*
 * param.c
 *
 * Sandbox is licensed under the GNU GPL, version 3 or later.
 * See the LICENSE.
 * Copyright (C) Kenneth B. Jensen 2021. All rights reserved.
 *
 * This file was developed with creating parameters for
 * minimal_image in mind.
 */

#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sandbox.h"
#include "param.h"
#include "std/util.h"

/*
 * configuration for mini-images
 */
const char *DEFAULT_PARAMS[] = {
	QEMU_SYSTEM_BIN,
	"-no-user-config",
	"-sandbox",
	"on,obsolete=deny,elevateprivileges=deny,"
		"spawn=deny,resourcecontrol=deny",
	"-nodefaults",
	"-nographic",
	"-no-reboot",
	"-no-shutdown",
	"-m", "64M",
	NULL
};

const char *console_template =
                "socket,id=serio,"
                "port=%d,host=localhost,server,nowait",
           *qmp_template =
                "tcp:localhost:%s,server,nowait";


/* 
 * This smelly pile of code creates a str array
 * for use with exec from a config file (config_path).
 * Most of it needs to be subbed out to a subroutine.
 */
char **
get_startparams(char *config_path, char *qmp_port, int console_port) {
	FILE            *config_file = NULL;
	char            **params = NULL,
	                *line = NULL;
	const char      **str = NULL;           /* const to shut GCC up */
	size_t          nparam = 0,
	                count = 0,
	                i = 0;
	int             rv = -1;

	if (NULL == config_path || NULL == qmp_port || 0 >= console_port) {
		return NULL;
	}

	config_file = fopen(config_path, "r");
	if (NULL == config_file) {
		FATAL("Could not open config file %s: %s",
		      config_path, errno_s);
		return NULL;
	}

	for (str = DEFAULT_PARAMS; NULL != *str; ++str) {
		growarray(params, nparam, sizeof(char*), 1)
		params[nparam-1] = strdup(*str);
	}

	while (0 < getline(&line, &count, config_file)) {
		growarray(params, nparam, sizeof(char*), 1)

		/* 
		 * getline()'s line endings will make for bad argv
		 */
		chop(line);
		params[nparam-1] = strdup(line);
	}

	
	growarray(params, nparam, sizeof(char*), 5)
	params[nparam-5] = strdupchk("-chardev");
	rv = asprintf(&params[nparam-4], console_template, console_port);
	if (0 > rv) {
		ERR("get_startparam: asprintf: %s", errno_s);
	}

	params[nparam-3] = strdupchk("-qmp");
	rv = asprintf(&params[nparam-2], qmp_template, qmp_port);
	if (0 > rv) {
		ERR("get_startparam: asprintf: %s", errno_s);
	}

	/* 
	 * exec() expects its array to be NULL-terminated. 
	 */
	params[nparam-1] = NULL;

	/* and sanity check. */
	checkarray(params, nparam-1, goto _get_startparam_err);

	return params;
_get_startparam_err:
	for (i = 0; i < nparam; ++i) {
		checkfree(params[i]);
	}
	checkfree(params);
	return NULL;
}

#undef chop
#undef growarray
