#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "irc.h"
#include "std/util.h"

void
freeircmsg(ircmsg_t *msg) {
	checkfree(msg->prefix);
	checkfree(msg->nick);
	checkfree(msg->user);
	checkfree(msg->host);
	checkfree(msg->command);
	checkfree(msg->middle);
	checkfree(msg->params);
	checkfree(msg->trailing);
	checkfree(msg);
}

int
vircsend(const char *fmt, va_list ap) {
	char msg[MSGMAXLEN] = {0};
	ssize_t len = -1;
	int rv = -1;

	len = vsnprintf(msg, MSGMAXLEN-2, fmt, ap);
	if (0 > len) {
		ERR("ircsend: vsnprintf: %s", strerror(errno));
		LOG(stderr, "fmt: %s", fmt);
		return -1;
	}

	/*
	 * we cannot append "\r\n" to the string in the sprintf call, do it the
	 * hard way.
	 */
	memcpy(&msg[len], "\r\n\0", 3);
	len += 2;
	_LOG(stdout, "%s %s", GETTIME(), msg);
	if (512 < len) {
		WARN("ircsend: message length is greater than 512!");
	}

	rv = send(ircfd, msg, len, 0);
	if (0 > rv) {
		ERR("ircsend: send: %s", strerror(errno));
	}

	return rv;
}

int
ircsend(const char *fmt, ...) {
	int rv = -1;
	va_list ap;

	va_start(ap, fmt);
	rv = vircsend(fmt, ap);
	va_end(ap);

	return rv;
}
int
ircfwdmsg(int ipcfd) {
	int             rv = -1;
	char            buf[MSGMAXLEN+1] = {0};

	rv = recv(ipcfd, buf, MSGMAXLEN, 0);
	if (0 > rv) {
		ERR("ircfwdmsg: recv: %s", errno_s);
		return -1;
	}

	return ircsend("%s", buf);

}
int
_SAY(char const *target, const char *fmt, ...) {
	int rv = -1;
	char buf[MSGMAXLEN] = "PRIVMSG %s :";
	va_list ap;

	assert(NULL != target);
	assert(NULL != fmt);

	rv = snprintf(buf, MSGMAXLEN, "PRIVMSG %s :%s", target, fmt);
	if (0 > rv) {
		ERR("say: snprintf: %s", strerror(errno));
		return rv;
	}

	va_start(ap, fmt);
	rv = vircsend(buf, ap);
	va_end(ap);

	return rv;
}
