#ifndef _SANDBOX_PLUGIN_
#define _SANDBOX_PLUGIN_
#include "../irc.h"

int sandbox_init(void);
int sandbox_run(ircmsg_t *msg, char *buf);
int sandbox_reset(void);
int sandbox_stop(void);

#endif /* _SANDBOX_PLUGIN_ */
