#include <errno.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "sandbox.h"
#include "../bot.h"
#include "../std/net.h"
#include "../std/util.h"

static int console_port = -1;

int
sandbox_init(void) {
	int             sock = -1,
	                rv = -1;
	char buf[MSGMAXLEN+1] = {0};

	sock = net_connect(SANDBOXD_HOST, SANDBOXD_PORT);
	if (0 > sock) {
		ERR("sandbox_init: Error connecting to sandboxd");
		return -1;
	}

	/*
	 * first recv() for the greeting message
	 */
	rv = recv(sock, buf, rv, 0);
	if (0 > rv) {
		ERR("sandbox_init: recv: %s", errno_s);
		return -1;
	}
	INFO("sandbox_init in: '%s'",  buf);

	/*
	 * only one send()
	 */
	rv = snprintf(buf, MSGMAXLEN, "start %s\n", SANDBOX_CONF);
	rv = send(sock, buf, rv, 0);
	if (0 > rv) {
		ERR("sandbox_init: send: %s", errno_s);
		return -1;
	}
	INFO("sandbox_init out: '%s'",  buf);

	/*
	 * second recv() for the response
	 */
	memset(buf, '\0', MSGMAXLEN);
	rv = recv(sock, buf, MSGMAXLEN, 0);
	if (0 > rv) {
		ERR("sandbox_init: recv: %s", errno_s);
		goto _sandbox_init_err;
	}
	INFO("sandbox_init in: '%s'",  buf);
	
	if (match(buf, "OK ")) {
		console_port = strtol(buf+3, NULL, 10);
	}

	rv = EXIT_SUCCESS;
_sandbox_init_err:
	net_close(sock);
	return rv;
}

int
sandbox_run(ircmsg_t *msg, char *resp) {
	/*
	 * 8 is long enough for PORT_MAX
	 */
	char            buf[1] = {0},
	                service[8] = {0},
	                *cmdline = NULL;
	int             sock = -1,
	                rv = -1,
	                total = 0,
	                linecount = 0;
	struct timeval  tv = {
	                        .tv_sec = 0,
	                        .tv_usec = 400000 /* .4s */
	                };

	if (NULL == msg->params) {
		ERR("sandbox_run: msg parameters is NULL!");
		return -1;
	}
	cmdline = &msg->params[3];

	snprintf(service, 8, "%d", console_port);

	sock = net_connect("localhost", service);
	if (0 > sock) {
		ERR("sandbox_run: net_connect(%s, %s)", "localhost", service);
		return -1;
	}

	/*
	 * set timeout on socket
	 */
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
	
	DEBUG("sandbox_run: '%s'", cmdline);
	rv = dprintf(sock, "%s\r\n", cmdline);
	if (0 > rv) {
		ERR("sandbox_run: dprintf: %s", errno_s);
		net_close(sock);
		return -1;
	}

	while (MSGMAXLEN > total && 7 > linecount) {
		/*
		 * PEEK because QEMU's socket console is temperamental
		 */
		rv = recv(sock, buf, 1, MSG_PEEK);
		if (0 > rv && EAGAIN == errno) {
			net_close(sock);
			return EXIT_SUCCESS;
		}
		else if (0 > rv) {
			ERR("sandbox_run: recv/peek: %s", errno_s);
			net_close(sock);
			return -1;
		}
		else if (0 == rv) {
			net_close(sock);
			return EXIT_SUCCESS;
		}
		
		rv = recv(sock, buf, 1, 0);
		if (0 > rv) {
			ERR("sandbox_run: recv: %s", errno_s);
			net_close(sock);
			return -1;
		}
		strncat(resp, buf, 1);
		total += 1;
		if ('\n' == buf[0]) {
			linecount += 1;
		}
		buf[0] = '\0';
	}

	net_close(sock);
	return EXIT_SUCCESS;
}

int
sandbox_reset(void) {
	int             sock = -1,
	                rv = -1;
	char buf[MSGMAXLEN+1] = {0};

	sock = net_connect(SANDBOXD_HOST, SANDBOXD_PORT);
	if (0 > sock) {
		ERR("sandbox_reset: Error connecting to sandboxd");
		return -1;
	}

	/*
	 * first recv() for the greeting message
	 */
	rv = recv(sock, buf, rv, 0);
	if (0 > rv) {
		ERR("sandbox_reset: recv: %s", errno_s);
		return -1;
	}
	INFO("sandbox_reset in: '%s'",  buf);

	/*
	 * only one send()
	 */
	rv = snprintf(buf, MSGMAXLEN, "reset %d\n", console_port);
	rv = send(sock, buf, rv, 0);
	if (0 > rv) {
		ERR("sandbox_reset: send: %s", errno_s);
		return -1;
	}
	INFO("sandbox_reset out: '%s'",  buf);

	/*
	 * second recv() for the response
	 */
	memset(buf, '\0', MSGMAXLEN);
	rv = recv(sock, buf, MSGMAXLEN, 0);
	if (0 > rv) {
		ERR("sandbox_reset: recv: %s", errno_s);
		goto _sandbox_reset_err;
	}
	INFO("sandbox_reset in: '%s'",  buf);
	
	if (match(buf, "OK ")) {
		console_port = strtol(buf+3, NULL, 10);
		rv = EXIT_SUCCESS;
	}
	else {
		rv = -1;
	}

_sandbox_reset_err:
	net_close(sock);
	return rv;
}

int
sandbox_stop(void) {
	int             sock = -1,
	                rv = -1;
	char buf[MSGMAXLEN+1] = {0};

	sock = net_connect(SANDBOXD_HOST, SANDBOXD_PORT);
	if (0 > sock) {
		ERR("sandbox_stop: Error connecting to sandboxd");
		return -1;
	}

	/*
	 * first recv() for the greeting message
	 */
	rv = recv(sock, buf, rv, 0);
	if (0 > rv) {
		ERR("sandbox_stop: recv: %s", errno_s);
		return -1;
	}
	INFO("sandbox_stop in: '%s'",  buf);

	/*
	 * only one send()
	 */
	rv = snprintf(buf, MSGMAXLEN, "stop %d\n", console_port);
	rv = send(sock, buf, rv, 0);
	if (0 > rv) {
		ERR("sandbox_stop: send: %s", errno_s);
		return -1;
	}
	INFO("sandbox_stop out: '%s'",  buf);

	/*
	 * second recv() for the response
	 */
	memset(buf, '\0', MSGMAXLEN);
	rv = recv(sock, buf, MSGMAXLEN, 0);
	if (0 > rv) {
		ERR("sandbox_stop: recv: %s", errno_s);
		goto _sandbox_stop_err;
	}
	INFO("sandbox_stop in: '%s'",  buf);
	
	if (match(buf, "OK")) {
		rv = EXIT_SUCCESS;
	}
	else {
		rv = -1;
	}
_sandbox_stop_err:
	net_close(sock);
	return rv;
}
