/*
 * shitbot - IRC user interface to execute arbitrary commands in a sandbox
 * Copyright (C) 2021 Kenneth B. Jensen <kj@0x5f3759df.xyz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.

 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "std/util.h"
#include "std/net.h"
#include "bot.h"
#include "irc.h"
#include "parse.h"
#include "sandbox.h"

extern int ircfd;
extern int console_port;

const char *irc_server = NULL;
const char *irc_portno = NULL;

_Noreturn void usage(int exitcode, char *argv0);
void sigterm_handler(int signal);
void dispatch(char *line, int n);
void shitbot(void);

_Noreturn void
usage(int exitcode, char *argv0) {
	printf("%s <irc.server.net> <portno>\n", argv0);
	exit(exitcode);
}

void 
sigterm_handler(int signal) {
	INFO("exiting with %d: %s", signal,
		(EXIT_SUCCESS == sandbox_stop()) ?
			"sandbox exit successful"       : 
			"sandbox exit error, see logs"
	);
}

int
main(int argc, char *argv[]) {
	const int optval = 1;

	int ipcfds[2] = {-1};
	int ircfd = -1;
	int rv = -1;

	if (3 > argc) {
		usage(EXIT_FAILURE, argv[0]);
	}

	irc_server_host = argv[1];
	irc_server_service = argv[2];

	INFO("shitbot %s started up, PID %d", 
	     SHITBOT_VERSION, getpid());
	
	rv = socketpair(AF_UNIX, SOCK_STREAM, 0, ipcfds);
	if (0 > rv) {
		ERR("main: socketpair: %s", errno_s);
		net_close(ircfd);
		return EXIT_FAILURE;
	}

	irc_p = pthread_create(&thread, NULL,
	                       do_irc, *ipcfds[0]);

	box_p = pthread_create(&thread, NULL,
	                       do_box, *ipcfds[1]);

	
}

void *
do_irc(void *_ipcfd) {
	const char *irc_server_host = NULL;
	const char *irc_server_service = NULL;
	const int ipcfd = *p;

	ircfd = net_connect(irc_server_host, 
	                    irc_server_service);
	if (0 > ircfd) {
		FATAL("main: net_connect failed.");
		return EXIT_FAILURE;
	}
	
	rv = setsockopt(ircfd, SOL_SOCKET, SO_KEEPALIVE,
	                &optval, sizeof(optval));
	if (0 > rv) {
		FATAL("main: setsockopt: %s", 
		      strerror(errno));
		net_close(ircfd);
		return EXIT_FAILURE;
	}
}
