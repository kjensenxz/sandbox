#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "bot.h"
#include "irc.h"
#include "parse.h"
#include "plugin.h"
#include "util.h"

#include "plugins/sandbox.h"

extern int ircfd;

void *
plugin_loop(void *_ipcfd) {
	ircmsg_t        *msg = NULL;
	char            buf[MSGMAXLEN+1] = {0},
	                *line = NULL;
	int             ipcfd = *(int *)_ipcfd,
	                rv = -1;

	INFO("plugin_loop started");
	rv = sandbox_init();
	if (0 > rv) {
		ERR("sandbox_init: could not init! is sandboxd running?");
	}

_plugin_loop:
	memset(buf, '\0', MSGMAXLEN);
	rv = recv(ipcfd, buf, MSGMAXLEN, 0);
	if (0 > rv) {
		FATAL("plugin_loop: ipc recv: %s", errno_s);
		return NULL;
	}

	msg = parseline(buf);
	if (NULL == msg) {
		ERR("plugin_loop: parsebuf!");
	}
	msg->target = match(msg->middle, mynick) ? msg->nick : msg->middle;

	if (match(msg->params, "#! reset")) {
		rv = sandbox_reset();
		if (0 > rv) {
			rv = sandbox_init();
			if (0 > rv) {
				say("Error resetting!");
			}
		}
	}
	else if (match(msg->params, "#! ")) {
		memset(buf, '\0', MSGMAXLEN);
		rv = sandbox_run(msg, buf);
		if (0 == rv) {
			line = strtok(buf, "\r\n");
			while (NULL != (line = strtok(NULL, "\r\n"))) {
				say(line);
			}
		}
		else {
			say("Error running command!");
		}
	}

	

	freeircmsg(msg);
	goto _plugin_loop;
}
