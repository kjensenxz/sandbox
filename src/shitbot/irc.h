#ifndef _IRC_H_
#define _IRC_H_
#include <stdarg.h>

typedef struct irc_message {
	char *prefix;
	char *nick;
	char *user;
	char *host;
	char *command;
	char *middle;
	char *params;
	char *trailing;
	char *target;
} ircmsg_t;

#define MSGMAXLEN               512
#define RPL_MYINFO              "004"
#define ERR_NICKNAMEINUSE       "433"

#define say(...) \
	_SAY(msg->target, __VA_ARGS__)

int ircfd;

void freeircmsg(ircmsg_t *msg);

int vircsend(const char *fmt, va_list ap);
int ircsend(const char *fmt, ...);
int ircfwdmsg(int ipcfd);
int _SAY(char const *target, const char *fmt, ...);


#endif /* _IRC_H */
