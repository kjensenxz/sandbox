/*
 * shitbot - IRC user interface to execute arbitrary commands in a sandbox
 * Copyright (C) 2021 Kenneth B. Jensen <kj@0x5f3759df.xyz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.

 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "std/util.h"
#include "std/net.h"
#include "bot.h"
#include "irc.h"
#include "parse.h"
#include "plugin.h"

#include "plugins/sandbox.h"

extern int ircfd;
static pthread_t plugin_td = {0};

_Noreturn void usage(int exitcode, char *argv0);
void sigterm_handler(int signal);

int bot_loop(int ipcfd);
int bot_do(int ipcfd);
int bot_dispatch(int ipcfd, char *line);

_Noreturn void
usage(int exitcode, char *argv0) {
	printf("%s <irc.server.net> <portno>\n", argv0);
	exit(exitcode);
}

void 
sigterm_handler(int signal) {
	int             rv = -1;

	INFO("recieved signal %d", signal);

	rv = sandbox_stop();
	pthread_cancel(plugin_td);

	INFO("exiting.");
}

#define MSGMAXLEN 512
int
bot_dispatch(int ipcfd, char *line) {
	ircmsg_t        *msg = NULL;
	const char      *cmd = NULL,
	                **p = NULL;
	int             rv = -1;
	
	msg = parseline(line);
	if (NULL == msg) {
		return -1;
	}

	/*
	 * target will be user (for messages PRIVMSG'd to bot) or channel
	 */
	if (NULL != msg->middle) {
		msg->target = match(msg->middle, mynick) ? msg->nick : msg->middle;
	}
	cmd = msg->command;

	if (match(cmd, "PRIVMSG")) {
		/*
		 * le ebin easter egg
		 */
		if (match(msg->params, "#! fsck off")) {
			say("Error running command: FUCK U 2 BUDDY");
		}
		else if (match(msg->params, "#! ")) {
			DEBUG("bot_dispatch: msg: %p", &msg);
			rv = send(ipcfd, line, strlen(line), 0);
			if (0 > rv) {
				say("Error running command: could not "
				    "communicate with IPC!");
			}
		}
		else if (match(msg->params, ".bots") || match(msg->params, "!bots")) {
			say("shitbot reporting in! [ISO C17, AGPLv3] "
			    "https://gitlab.com/kjensenxz/sandbox");
		}
		else if (match(msg->params, ".cmds") ||
		    match(msg->params, "!cmds")      ||
	            match(msg->params, ".commands")  ||
		    match(msg->params, "!commands")  ||
		    match(msg->params, ".help")      || 
		    match(msg->params, "!help")) {
			say("Execute shell commands with #! <command>");
		}
		else if (match(msg->params, ".host") || match(msg->params, "!host")) {
			say("%s!%s@%s", msg->nick, msg->user, msg->host);
		}
		else if (match(msg->params, "\x01VERSION\x01")) {
			ircsend("NOTICE %s :\x01VERSION shitbot %s\x01",
			        msg->nick, SHITBOT_VERSION);
		}
	}
	else if (match(cmd, "PING")) {
		ircsend("PONG :%s", msg->params);
	}
	else if (match(cmd, "NOTICE")) {
		if (NULL != strstr(msg->params, "Found your hostname") ||
		    NULL != strstr(msg->params, "Couldn't look up your hostname")) {
			ircsend("NICK %s", mynick);
			ircsend("USER %s * * :%s", mynick, mynick);
		}
	}
	else if (match(cmd, "INVITE")) {
		ircsend("JOIN %s", msg->params);
	}
	else if (match(cmd, RPL_MYINFO)) {
		ircsend("PRIVMSG NickServ :identify %s", nickservpass);
		for (p = startchannels; NULL != *p; ++p) {
			ircsend("JOIN %s", *p);
		}
	}
	else if (match(cmd, ERR_NICKNAMEINUSE)) {
		ircsend("NICK %s_", mynick);
		ircsend("USER %s * * :%s", mynick, mynick);
	}

	freeircmsg(msg);
	return EXIT_SUCCESS;
}
int
bot_do(int ipcfd) {
	char            peek[MSGMAXLEN+1] = {0},
	                line[MSGMAXLEN+1] = {0};
	char            *p = NULL;
	int             n = -1,
	                rv = -1;
	
	rv = recv(ircfd, peek, MSGMAXLEN, MSG_PEEK);
	if (0 > rv) {
		ERR("bot_do: recv/peek: %s", ircfd, errno_s);
		return -1;
	}

	/*
	 * peek ahead to find the first line, if batched
	 */
	p = index(peek, '\n');
	if (NULL == p) {
		return EXIT_SUCCESS;
	}
	n = p - peek + 1;
	if (0 >= n) {
		ERR("bot_do: bad peek msg!");
		DEBUG("bot_do: peek: '%s'", peek);
		DEBUG("bot_do: peek: %p, p: %p, n: %d", &peek, p, n);
		return -1;
	}

	rv = recv(ircfd, line, n, 0);
	if (0 > rv) {
		ERR("bot_do: recv: %s", errno_s);
		return -1;
	}
	else if (0 == rv) {
		ERR("bot_do: recv: Disconnected from IRC");
		return -1;
	}

	_LOG(stdout, "%s %s", GETTIME(), line);
	return bot_dispatch(ipcfd, line);
}

int
bot_loop(int ipcfd) {
	struct pollfd           fds[2] = {0};
	const nfds_t            nfds = 2;
	int                     rv = -1;
	/*
	 * long unsigned int to silence gcc complaining about comparing
	 * int vs nfds_t (const long unsigned int)
	 */
	long unsigned int       i = -1;

	fds[0].fd = ircfd;
	fds[0].events = POLLIN;
	fds[1].fd = ipcfd;
	fds[1].events = POLLIN;
	
_bot_loop:
	rv = poll(fds, nfds, -1);
	if (0 > rv) {
		FATAL("bot_loop: poll: %s", errno_s);
		return -1;
	}

	for (i = 0; i < nfds; ++i) {
		if (fds[i].revents & POLLIN) {
			if (ircfd == fds[i].fd) {
				rv = bot_do(ipcfd);
			}
			else if (ipcfd == fds[i].fd) {
				rv = ircfwdmsg(ipcfd);
			}

			if (0 > rv) {
				return -1;
			}
		}
		if (fds[i].revents & POLLPRI) {
			INFO("OOB data on socket %d...", fds[i].fd);
		}
		if (fds[i].revents & POLLHUP || fds[i].revents & POLLERR) {
			ERR("fd %d disconnected", fds[i].fd);
			return -1;
		}
	}

	goto _bot_loop;
}

int
main(int argc, char **argv) {
	int             ipcfd[2] = {-1},
	                rv = -1;
	
	char            *host = NULL,
	                *service = NULL;
	
	if (3 > argc) {
		usage(EXIT_FAILURE, argv[0]);
		goto _main_err;
	}
	
	host = argv[1];
	service = argv[2];

	INFO("shitbot %s started", SHITBOT_VERSION);
	signal(SIGINT, sigterm_handler);
	signal(SIGTERM, sigterm_handler);

	rv = socketpair(AF_UNIX, SOCK_STREAM, 0, ipcfd);
	if (0 > rv) {
		FATAL("main: socketpair: %s", errno_s);
		goto _main_err;
	}
	
	rv = pthread_create(&plugin_td, NULL, plugin_loop, &ipcfd[1]);
	if (0 > rv) {
		FATAL("main: pthread_create: %s", errno_s);
		goto _main_err;
	}

	ircfd = net_connect(host, service);
	if (0 > rv) {
		FATAL("main: net_connect: %s", errno_s);
		goto _main_err;
	}

	rv = bot_loop(ipcfd[0]);
	pthread_join(plugin_td, NULL);

_main_err:
	if (0 > rv) {
		rv = EXIT_FAILURE;
	}
	net_close(ipcfd[0]);
	net_close(ipcfd[1]);
	net_close(ircfd);
	return rv;
}
