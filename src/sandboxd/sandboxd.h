#ifndef _SANDBOXD_H_
#define _SANDBOXD_H_

// TODO: make these damn things into args... or a damn config file
#define PORT_MIN        1024
#define PORT_MAX        1048
#define SANDBOXD_DBPATH "/home/kbjensen/sandbox/src/sandboxd/sandboxd.sqlite3"
#define SANDBOX_PATH    "/home/kbjensen/sandbox/src/sandbox/sandbox"


#define RESET_ARGC      6
#define START_ARGC      6
#define STOP_ARGC       4


enum sandbox_cmds {
	SANDBOX_START,
	SANDBOX_RESET,
	SANDBOX_STOP
};

typedef struct instruction {
	int cmd;
	char *arg;
} instruction_t;

#endif /* _SANDBOXD_H_ */
