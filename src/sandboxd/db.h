#ifndef _DB_H_
#define _DB_H_

int db_init(void);
int db_getstartparam(int *qmp_port, int *console_port);
int db_getresetparam(char *console_port, char **config_name, int *qmp_port);
int db_getstopparam(char *console_port, int *qmp_port);
int db_getpid(char *console_port);
int db_update(int cmd, char **argv);

#endif /* _DB_H_ */
