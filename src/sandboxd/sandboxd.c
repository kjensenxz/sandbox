#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>

#include "db.h"
#include "sandboxd.h"
#include "std/net.h"
#include "std/util.h"

#define BUFLEN 256

/* 
 * == TODO ==
 * - check all TODO/XXX labels
 * - fix error reporting and argv leak with reset/stop on invalid port
 */

_Noreturn void usage(char *argv0, int exitcode);


static inline int tdprintf(int fd, const char *fmt, ...);
instruction_t *get_instruction(char *line);

char **getstartargv(char *config_path);
char **getresetargv(char *console_port, int *stop_pid);
char **getstopargv(char *console_port, int *stop_pid);

_Noreturn void child_worker(int clientfd);

_Noreturn void
usage(char *argv0, int exitcode) {
	printf(
	"%s <portno>\n",
	argv0);

	exit(exitcode);
}

/*
 * get_instruction returns an instruction_t loaded up from the format
 *     <command> <space> <argument> <newline>
 *     e.g. "start /home/kbjensen/sandbox/minimal_image.sandbox
 * or NULL on system error.
 */
instruction_t *
get_instruction(char *line) {
	instruction_t   *instruction = NULL;
	char            *space = NULL,
	                *newline = NULL,
	                *cmd = NULL;

	if (NULL == line) {
		ERR("get_instruction: null arguments!");
		return NULL;
	}

	instruction = calloc(1, sizeof(instruction_t));
	if (NULL == instruction) {
		FATAL("get_instruction: calloc: %s", errno_s);
		return NULL;
	}

	instruction->cmd = -1;
	instruction->arg = NULL;
	space = index(line, ' ');
	if (NULL == space || line > space) {
		ERR("get_instruction: Unable to parse client data.");
		goto _get_instruction_error;
	}

	/* 
	 * space minus line_begin -> length of first word (cmd)
	 * gcc likes to bitch about conversion errors
	 */
        #define LITTLE63BITS(VALUE) \
	                ((u_int64_t)(VALUE) & 0x7FFFFFFFFFFFFFFF)
	u_int64_t       len = 0;
	len = LITTLE63BITS(&space) - LITTLE63BITS(&line);
	cmd = strndup(line, len);
	if (NULL == cmd) {
		ERR("get_instruction: Unable to parse client data.");
		goto _get_instruction_error;
	}
	else if (match(cmd, "start")) {
		instruction->cmd = SANDBOX_START;
	}
	else if (match(cmd, "reset")) {
		instruction->cmd = SANDBOX_RESET;
	}
	else if (match(cmd, "stop")) {
		instruction->cmd = SANDBOX_STOP;
	}
	else {
		ERR("get_instruction: bad cmd %s", cmd);
		goto _get_instruction_error;
	}
	free(cmd);
	
	/* 
	 * silence compiler errors with bit fuckery,
	 * index(line, \n)-> newline
	 * read at offset: space-line+1 to newline -> second word
	 * newline-space-1 -> second word length without newline (arg)
	 */
	newline = index(line, '\n');
	if (NULL == newline) {
		ERR("get_instruction: malformed request no newline");
		goto _get_instruction_error;
	}
	len = (LITTLE63BITS(newline) - LITTLE63BITS(space) - 1);
	//DEBUG("get_instruction: len: %d, newline %p, line %p, newline-line %d ", len, &newline, &space, newline-line);
	if (0 == len || BUFLEN < len) {
		ERR("get_instruction: len is %d!", len);
		goto _get_instruction_error;
	}

	instruction->arg = strndup(&line[space-line+1], len);
	if (NULL == instruction->arg) {
		ERR("get_instruction: Unable to parse client data.");
		goto _get_instruction_error;
	}
	
	//DEBUG("get_instruction: arg: '%s'", instruction->arg);

	return instruction;

_get_instruction_error:
	checkfree(cmd);
	checkfree(instruction->arg);
	checkfree(instruction);
	return NULL;
}

char **
getstartargv(char *config_path) {
	int             qmp_port = -1;
	int             console_port = -1;

	int             rv = -1;
	char            **argv = NULL;
	
	if (NULL == config_path) {
		ERR("getstartargv: config_path is NULL.");
		return NULL;
	}

	rv = access(config_path, R_OK);
	if (0 > rv) {
		ERR("getstartargv: access(%s): %s",
		    config_path, errno_s);
		return NULL;
	}

	argv = calloc(START_ARGC, sizeof(char *));
	if (NULL == argv) {
		FATAL("getstartargv: bad calloc: %s", errno_s);
		return NULL;
	}

	rv = db_getstartparam(&qmp_port, &console_port);
	if (EXIT_SUCCESS != rv) {
		FATAL("getstartargv: db_getstartparam");
		free(argv);
		return NULL;
	}
	DEBUG("getstartargv: qmp_port: %d, console_port: %d", 
	      qmp_port, console_port);

	argv[0] = strdup(SANDBOX_PATH);
	argv[1] = strdup("start");
	argv[2] = strdup(config_path);
	rv = asprintf(&argv[3], "%d", qmp_port);
	rv = asprintf(&argv[4], "%d", console_port);

	/* -1 to omit the NULL */
	checkarray(argv, START_ARGC-1, goto _getstartargv_err);
	return argv;

_getstartargv_err:
	freearray(argv, START_ARGC);
	return NULL;
}

char **
getresetargv(char *console_port, int *stop_pid) {
	char            *config_path = NULL,
	                **argv = NULL;
	int             qmp_port = -1,
	                rv = -1;
	
	if (NULL == console_port) {
		ERR("getresetargv: console_port is NULL.");
		return NULL;
	}

	argv = calloc(RESET_ARGC, sizeof(char *));
	if (NULL == argv) {
		FATAL("getresetargv: bad calloc: %s", errno_s);
		return NULL;
	}

	*stop_pid = db_getpid(console_port);
	if (0 > *stop_pid) {
		ERR("getstopargv: db_getpid");
		free(argv);
		return NULL;
	}

	rv = db_getresetparam(console_port, &config_path, &qmp_port);
	if (EXIT_SUCCESS != rv) {
		FATAL("getresetargv: db_getresetparam");
		free(argv);
		return NULL;
	}
	/* TODO: check out rv, config_path */
	DEBUG("getresetargv: config_path: %s (%p), qmp_port: %d (%p)", config_path, &config_path, qmp_port, &qmp_port);

	argv[0] = strdup(SANDBOX_PATH);
	argv[1] = strdup("reset");
	argv[2] = config_path;
	rv = asprintf(&argv[3], "%d", qmp_port);
	argv[4] = strdup(console_port);

	checkarray(argv, RESET_ARGC-1, goto _getresetargv_err);
	return argv;

_getresetargv_err:
	freearray(argv, RESET_ARGC);
	return NULL;
}

char **
getstopargv(char *console_port, int *stop_pid) {
	char            **argv = NULL;
	int             qmp_port = -1,
	                rv = -1;
	
	if (NULL == console_port) {
		ERR("getstopargv: console_port is NULL.");
		return NULL;
	}

	argv = calloc(STOP_ARGC, sizeof(char *));
	if (NULL == argv) {
		FATAL("getstopargv: bad calloc: %s", errno_s);
		return NULL;
	}

	*stop_pid = db_getpid(console_port);
	if (0 > *stop_pid) {
		ERR("getstopargv: db_getpid");
		free(argv);
		return NULL;
	}

	rv = db_getstopparam(console_port, &qmp_port);
	if (EXIT_SUCCESS != rv) {
		ERR("getstopargv: db_getstopparam ");
		free(argv);
		return NULL;
	}
	/* TODO: check out rv, config_path */
	DEBUG("getstopargv: qmp_port: %d", qmp_port);

	argv[0] = strdup(SANDBOX_PATH);
	argv[1] = strdup("stop");
	rv = asprintf(&argv[2], "%d", qmp_port);

	checkarray(argv, STOP_ARGC-1, goto _getstopargv_err);
	return argv;

_getstopargv_err:
	freearray(argv, STOP_ARGC);
	return NULL;
}

_Noreturn void
child_worker(int clientfd) {
	instruction_t   *instruction = NULL;
	char            *line = NULL,
	                *resp = NULL,
	                **argv = NULL;
	int             stop_pid = -1;
	ssize_t         rv = -1;

	line = calloc(BUFLEN, sizeof(char));
	if (NULL == line) {
		FATAL("child_worker: unable to calloc: %s", errno_s);
		tdprintf(clientfd, "ERR System error!\n");
		net_close(clientfd);
		exit(EXIT_FAILURE);
	}

	tdprintf(clientfd, "sandboxd 0.1.0 ready\n");
	rv = recv(clientfd, line, BUFLEN-1, 0);
	if (0 >= rv) {
		WARN("child_worker: recv: %s", 
		     0 == rv ? "returned 0"
		              : errno_s);
		tdprintf(clientfd, "ERR Read error!\n");
		checkfree(line);
		net_close(clientfd);
		exit(EXIT_FAILURE);
	}

	INFO("child_worker: incoming: %s", line);

	instruction = get_instruction(line);
	checkfree(line);
	if (NULL == instruction) {
		ERR("child_worker: Bad instruction!");
		tdprintf(clientfd, "ERR Bad instruction!\n");
		net_close(clientfd);
		exit(EXIT_FAILURE);
	}

	switch (instruction->cmd) {
		case SANDBOX_RESET:
			argv = getresetargv(instruction->arg, &stop_pid);
			break;
		case SANDBOX_START:
			argv = getstartargv(instruction->arg);
			break;
		case SANDBOX_STOP:
			argv = getstopargv(instruction->arg, &stop_pid);
			break;
		default:
			assert(0);
	}

	if (NULL == argv) {
		/* TODO: better error reporting */
		ERR("child_worker: bad argv!");
		tdprintf(clientfd, "ERR Bad argv or system error!\n");
		checkfree(instruction->arg);
		checkfree(instruction);
		net_close(clientfd);
		exit(EXIT_FAILURE);
	}

	/* XXX: db_update will get PID if needed */
	rv = db_update(instruction->cmd, argv);
	if (EXIT_SUCCESS != rv) {
		ERR("child_worker: bad db_update!");
		tdprintf(clientfd, "ERR Database error!\n");
		checkfree(instruction->arg);
		checkfree(instruction);
		switch (instruction->cmd) {
			case SANDBOX_RESET:
				freearray(argv, RESET_ARGC);
				break;
			case SANDBOX_START:
				freearray(argv, START_ARGC);
				break;
			case SANDBOX_STOP:
				freearray(argv, STOP_ARGC);
				break;
		}
		net_close(clientfd);
		exit(EXIT_FAILURE);
	}

	switch (instruction->cmd) {
		case SANDBOX_START:
		case SANDBOX_RESET:
			DEBUG("child_worker: argv: [%s, %s, %s, %s, %s]", argv[0], argv[1], argv[2], argv[3], argv[4]);
			/*
			 * argv[4] is the console_port.
			 * TODO: move code into main() to check for execv
			 */
			rv = asprintf(&resp, "OK %s\n", argv[4]);
			if (0 > rv) {
				ERR("child_worker: asprintf: %s", errno_s);
				checkfree(instruction->arg);
				checkfree(instruction);
				tdprintf(clientfd, "ERR System error!");
				net_close(clientfd);
				exit(EXIT_FAILURE);
			}
			nwrite(clientfd, resp);
			LOG(stdout, "%s", resp);
			free(resp);
			break;
		case SANDBOX_STOP:
			tdprintf(clientfd, "OK\n");
			break;
	}

	checkfree(instruction->arg);
	checkfree(instruction);

	net_close(clientfd);
	execv(SANDBOX_PATH, argv);
	FATAL("child_worker: unable to exec!");
	exit(EXIT_FAILURE);
}

int
main(int argc, char **argv) {
	char *          service = argv[1];
	pid_t           pid = 0;
	int             serverfd = -1,
	                clientfd = -1,
	                rv = -1;

	if (2 > argc) {
		usage(argv[0], EXIT_FAILURE);
	}

	INFO("sandboxd started");

	rv = db_init();
	if (EXIT_SUCCESS != rv) {
		FATAL("main: bad db_init!");
		return EXIT_FAILURE;
	}

	serverfd = net_listen("localhost", service);
	if (0 > serverfd) {
		net_close(serverfd);
		FATAL("main: net_listen: %s", errno_s);
		return EXIT_FAILURE;
	}

	fcntl(serverfd, F_SETFL, O_NONBLOCK);
	INFO("listening on :%s", service);

_main_loop:
	clientfd = net_accept4(serverfd, SOCK_CLOEXEC);
	if (0 > clientfd && (EWOULDBLOCK != errno || EAGAIN != errno)) {
		FATAL("main: net_accept4: %s", errno_s);
		net_close(clientfd);
		net_close(serverfd);
		return EXIT_FAILURE;
	}
	else if (0 < clientfd) {
		// TODO: print peer
		pid = fork();
		if (0 > pid) {
			FATAL("main: fork: %s", errno_s);
			net_close(clientfd);
			net_close(serverfd);
			return EXIT_FAILURE;
		}
		else if (0 == pid) {    /* child */
			net_close(serverfd);
			child_worker(clientfd);
			exit(EXIT_FAILURE);
		}
	}
	if (0 < pid) {
		INFO("main: PID %d started.", pid);
	}
	net_close(clientfd);
	pid = waitpid(-1, NULL, WNOHANG);
	if (0 < pid) {
		INFO("main: PID %d exited.", pid);
	}

	usleep(750000); /* .75ms */

	goto _main_loop;

	net_close(serverfd);
	return EXIT_SUCCESS;
}
