#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "db.h"
#include "sandboxd.h"
#include "util.h"

/*
 * db_init creates the database, if it does not exist.
 * called by main.
 * returns EXIT_SUCCESS or EXIT_FAILURE on error.
 */
int
db_init(void) {
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;

	const char *sql1 = 
		"CREATE TABLE IF NOT EXISTS 'sandboxes' ("
			"pid INTEGER UNIQUE NOT NULL, "
			"config_name VARCHAR(255) NOT NULL, "
			"qmp_port SMALLINT UNIQUE NOT NULL, "
			"console_port SMALLINT UNIQUE NOT NULL, "
			"creation TIMESTAMP WITH TIME ZONE NOT NULL"
		");";

	const char *sql2 = 
		"DELETE FROM 'sandboxes';";

	int rv = -1;
	
	rv = sqlite3_open_v2(SANDBOXD_DBPATH, &db, 
	                     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	if (SQLITE_OK != rv || NULL == db) {
		FATAL("db_init: unable to open db: %s", sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_prepare_v2(db, sql1, strlen(sql1), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_init: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_step(stmt);
	if (SQLITE_DONE != rv) {
		FATAL("db_init: incomplete sqlite3_step: %s",
		      sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_init: sqlite3_finalize: %s", sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_prepare_v2(db, sql2, strlen(sql2), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_init: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_step(stmt);
	if (SQLITE_DONE != rv) {
		FATAL("db_init: incomplete sqlite3_step: %s",
		      sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_init: sqlite3_finalize: %s", sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	rv = sqlite3_close_v2(db);
	if (SQLITE_OK != rv) {
		FATAL("db_init: sqlite3_close_v2: %s", sqlite3_errstr(rv));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/*
 * db_getstartparam retrieves the next ports available for qmp_port and
 * console_port for use in getstartargv.
 * returns EXIT_SUCCESS or EXIT_FAILURE on error.
 */
int
db_getstartparam(int *qmp_port, int *console_port) {
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;

	/*
	 * +2 for the next set of ports...
	 */
	const char *sql_template =
		"SELECT "
			"MIN(%d, MAX(qmp_port)+2), "
			"MIN(%d, MAX(console_port)+2) " 
		"FROM "
			"'sandboxes'"
		";";

	char *sql = NULL;
	int rv = -1;

	rv = asprintf(&sql, sql_template, PORT_MAX, PORT_MAX+1);
	if (0 > rv || NULL == sql) {
		FATAL("db_getstartparam: asprintf: %s", strerror(errno));
		checkfree(sql);
		return EXIT_FAILURE;
	}
	DEBUG("db_getstartparam: %s", sql);

	rv = sqlite3_open_v2(SANDBOXD_DBPATH, &db, 
	                     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	if (SQLITE_OK != rv || NULL == db) {
		FATAL("db_getstartparam: unable to open db: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_getstartparam: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	free(sql);

	rv = sqlite3_step(stmt);
//	if (SQLITE_DONE == rv) {
//		*qmp_port = PORT_MIN;
//		*console_port = PORT_MIN+1;
//	}
	if (SQLITE_ROW == rv) {
		printf("SQLITE_ROW!!!\n");
		*qmp_port = sqlite3_column_int(stmt, 0);
		*console_port = sqlite3_column_int(stmt, 1);
	}
	else {
		FATAL("db_getstartparam: sqlite3_step: %s",
		      sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	if (0 == *qmp_port || 0 == *console_port) {
		*qmp_port = PORT_MIN;
		*console_port = PORT_MIN+1;
	}
	DEBUG("db_getstartparam: qmp_port: %d, console_port: %d", *qmp_port, *console_port);

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_getstartparam: sqlite3_finalize: %s",
		      sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	rv = sqlite3_close_v2(db);
	if (SQLITE_OK != rv) {
		FATAL("db_getstartparam: sqlite3_close_v2: %s",
		sqlite3_errstr(rv));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/*
 * db_getresetparam finds the sandbox with console_port, and retrieves the
 * needed config_name and qmp_port for getresetargv.
 * returns EXIT_SUCCESS or EXIT_FAILURE on error.
 */
int
db_getresetparam(char *console_port, char **config_name, int *qmp_port) {
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;

	const char *sql_template =
		"SELECT "
			"config_name, "
			"qmp_port "
		"FROM "
			"'sandboxes' "
		"WHERE "
			"console_port = '%s'"
		";";

	char *sql = NULL;
	int rv = -1;

	assert(NULL != console_port);

	rv = asprintf(&sql, sql_template, console_port);
	if (0 > rv || NULL == sql) {
		FATAL("db_getresetparam: asprintf: %s", strerror(errno));
		checkfree(sql);
		return EXIT_FAILURE;
	}

	rv = sqlite3_open_v2(SANDBOXD_DBPATH, &db, 
	                     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	if (SQLITE_OK != rv || NULL == db) {
		FATAL("db_getresetparam: unable to open db: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_getresetparam: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	free(sql);

	rv = sqlite3_step(stmt);
	if (SQLITE_ROW == rv) {
		/* XXX: check!! */
		*config_name = strdup(sqlite3_column_text(stmt, 0));
		*qmp_port = sqlite3_column_int(stmt, 1);
	}
	else {
		FATAL("db_getresetparam: sqlite3_step: %s",
		      sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	DEBUG("db_getresetparam: config_name: '%s', qmp_port: %d", *config_name, *qmp_port);

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_getresetparam: sqlite3_finalize: %s",
		      sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	rv = sqlite3_close_v2(db);
	if (SQLITE_OK != rv) {
		FATAL("db_getresetparam: sqlite3_close_v2: %s",
		sqlite3_errstr(rv));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/*
 * db_getstopparam finds the sandbox with console_port, and retrieves the needed
 * qmp_port for getstopargv.
 * returns EXIT_SUCCESS or EXIT_FAILURE on error.
 */
int
db_getstopparam(char *console_port, int *qmp_port) {
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;

	const char *sql_template =
		"SELECT "
			"qmp_port "
		"FROM "
			"'sandboxes' "
		"WHERE "
			"console_port = '%s'"
		";";

	char *sql = NULL;
	int rv = -1;

	assert(NULL != console_port);

	rv = asprintf(&sql, sql_template, console_port);
	if (0 > rv || NULL == sql) {
		FATAL("db_getstopparam: asprintf: %s", strerror(errno));
		checkfree(sql);
		return EXIT_FAILURE;
	}

	rv = sqlite3_open_v2(SANDBOXD_DBPATH, &db, 
	                     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	if (SQLITE_OK != rv || NULL == db) {
		FATAL("db_getstopparam: unable to open db: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_getstopparam: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	free(sql);

	rv = sqlite3_step(stmt);
	if (SQLITE_ROW == rv) {
		*qmp_port = sqlite3_column_int(stmt, 0);
	}
	else {
		FATAL("db_getstopparam: sqlite3_step: %s",
		      sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	DEBUG("db_getstopparam: qmp_port: %d", *qmp_port);

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_getstopparam: sqlite3_finalize: %s",
		      sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	rv = sqlite3_close_v2(db);
	if (SQLITE_OK != rv) {
		FATAL("db_getstopparam: sqlite3_close_v2: %s",
		sqlite3_errstr(rv));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;

}

/*
 * db_getpid finds the sandbox with console_port, and retrieves the needed
 * pid to make sure we can kill the zombies
 * returns a pid or -1 on error
 */
int
db_getpid(char *console_port) {
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;

	const char *sql_template =
		"SELECT "
			"pid "
		"FROM "
			"'sandboxes' "
		"WHERE "
			"console_port = '%s'"
		";";

	char *sql = NULL;
	int rv = -1;
	int pid = -1;

	assert(NULL != console_port);

	rv = asprintf(&sql, sql_template, console_port);
	if (0 > rv || NULL == sql) {
		FATAL("db_getpid: asprintf: %s", strerror(errno));
		checkfree(sql);
		return -1;
	}

	rv = sqlite3_open_v2(SANDBOXD_DBPATH, &db, 
	                     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	if (SQLITE_OK != rv || NULL == db) {
		FATAL("db_getpid: unable to open db: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_close_v2(db);
		return -1;
	}

	rv = sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_getpid: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return -1;
	}

	free(sql);

	rv = sqlite3_step(stmt);
	if (SQLITE_ROW == rv) {
		pid = sqlite3_column_int(stmt, 0);
	}
	else {
		FATAL("db_getpid: sqlite3_step: %s",
		      sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return -1;
	}
	DEBUG("db_getpid: pid: %d", pid);

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_getpid: sqlite3_finalize: %s",
		      sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return -1;
	}
	rv = sqlite3_close_v2(db);
	if (SQLITE_OK != rv) {
		FATAL("db_getpid: sqlite3_close_v2: %s",
		sqlite3_errstr(rv));
		return -1;
	}
	return pid;
}

/*
 * db_update inserts on a start, deletes on a stop, and updates the PID for a reset for child_worker.
 * returns EXIT_SUCCESS or EXIT_FAILURE on error.
 */
int
db_update(int cmd, char **argv) {
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;

	const char *sql_template = NULL;
	int rv = -1;

	char *sql = NULL;

	switch (cmd) {
		/* START/RESET:
		 *     argv[2]: config_name
		 *         [3]: qmp_port
		 *         [4]: console_port
		 * STOP:
		 *     argv[2]: console_port
		 */
		case SANDBOX_START:
			sql_template = 
				"INSERT INTO 'sandboxes' VALUES ("
					"%d, '%s', %s, %s, datetime('now')"
				");";
			rv = asprintf(&sql, sql_template, getpid(),
			              argv[2], argv[3], argv[4]);
			break;
		case SANDBOX_RESET:
			sql_template =
				"UPDATE "
					"'sandboxes' "
				"SET "
					"PID = %d "
				"WHERE "
					"qmp_port = %s"
				";";
			rv = asprintf(&sql, sql_template, getpid(), argv[3]);
			break;
		case SANDBOX_STOP:
			sql_template = 
				"DELETE FROM "
					"'sandboxes' "
				"WHERE "
					"qmp_port = %s"
				";";
			rv = asprintf(&sql, sql_template, argv[2]);
			break;
		default:
			FATAL("BUG: db_update, invalid cmd");
			return EXIT_FAILURE;
	}

	if (0 > rv || NULL == sql) {
		FATAL("db_update: asprintf: %s", strerror(errno));
		checkfree(sql);
		return EXIT_FAILURE;
	}
	DEBUG("db_update: %s", sql);
	
	rv = sqlite3_open_v2(SANDBOXD_DBPATH, &db, 
	                     SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	if (SQLITE_OK != rv || NULL == db) {
		FATAL("db_update: unable to open db: %s", sqlite3_errstr(rv));
		free(sql);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
	if (SQLITE_OK != rv) {
		FATAL("db_update: unable to prepare statement: %s",
		      sqlite3_errstr(rv));
		free(sql);
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	free(sql);

	rv = sqlite3_step(stmt);
	if (SQLITE_DONE != rv) {
		FATAL("db_update: sqlite3_step: %s", sqlite3_errstr(rv));
		sqlite3_finalize(stmt);
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}

	rv = sqlite3_finalize(stmt);
	if (SQLITE_OK != rv) {
		FATAL("db_update: sqlite3_finalize: %s", sqlite3_errstr(rv));
		sqlite3_close_v2(db);
		return EXIT_FAILURE;
	}
	rv = sqlite3_close_v2(db);
	if (SQLITE_OK != rv) {
		FATAL("db_update: sqlite3_close_v2: %s", sqlite3_errstr(rv));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
